CC=gcc

SDL2INC=$(shell pkg-config --cflags sdl2)
SDL2LIB=$(shell pkg-config --libs sdl2 SDL2_image)

CFLAGS=-Wall -Wpedantic -O0 -g ${SDL2INC}
LDFLAGS=-lm ${SDL2LIB}

asteroids: asteroids.cpp
	${CC} ${CFLAGS} -o $@ $< ${LDFLAGS}

clean:
	rm asteroids
