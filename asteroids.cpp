#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <assert.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

const float RADIAN_TO_DEGREES = 180.0f / M_PI;
const float DEGREES_TO_RADIANS = M_PI / 180.0f;

const int WINDOW_WIDTH = 1280;
const int WINDOW_HEIGHT = 720;
const int DESIRED_FPS = 60;
const float DESIRED_DELTA = 1.0f / DESIRED_FPS;

SDL_Window *window;
SDL_Renderer *renderer;
SDL_Texture *texture_atlas;

const int BACKGROUND_LAYERS_COUNT = 6;
struct {
	SDL_Texture *tex;
	int w, h;
} background_layers[BACKGROUND_LAYERS_COUNT];
const int LAYER_MARGIN_X_INCREMENT = 15;
const int LAYER_MARGIN_Y_INCREMENT = 8;

bool running;

float mouse_x;
float mouse_y;
struct {
	bool down;
	bool pressed;
	bool released;
} keys[256];

float player_x = 0.1f;
float player_y = 0.1f;
float player_dx;
float player_dy;
const float PLAYER_SPEED = 0.7f;
const int PLAYER_SPRITE_WIDTH = 32;
const int PLAYER_SPRITE_HEIGHT = 32;

float thrust_x = 0.1f;
float thrust_y = 0.1f;
const int THRUST_SPRITE_WIDTH = 32;
const int THRUST_SPRITE_HEIGHT = 32;
const int THRUST_ANIMATION_FRAMES = 4;
const float THURST_ANIMATION_DELTA = 1.0f / THRUST_ANIMATION_FRAMES;

void clear(float r, float g, float b) {
	SDL_SetRenderDrawColor(renderer, r * 255, g * 255, b * 255, 255);
	SDL_RenderClear(renderer);
}

void line(float x1, float y1, float x2, float y2, float r, float g, float b) {
	SDL_SetRenderDrawColor(renderer, r * 255, g * 255, b * 255, 255);
	SDL_RenderDrawLine(renderer,
		(x1 + 1.0f) / 2.0f * WINDOW_WIDTH, WINDOW_HEIGHT - (y1 + 1.0f) / 2.0f * WINDOW_HEIGHT,
		(x2 + 1.0f) / 2.0f * WINDOW_WIDTH, WINDOW_HEIGHT - (y2 + 1.0f) / 2.0f * WINDOW_HEIGHT);
}

void player(float x, float y, float angle, float thrust_x, float thrust_y, bool animate_trust, int animation_frame) {
	printf("%f %f\n", angle, -angle + 90.0f);
	thrust_x = x + 0.05f * cosf((-angle + 360.0f) * DEGREES_TO_RADIANS);
	thrust_y = y + 0.05f * sinf((-angle + 360.0f) * DEGREES_TO_RADIANS);

	x = (x + 1.0f) / 2.0f * WINDOW_WIDTH - PLAYER_SPRITE_WIDTH / 2.0f;
	y = WINDOW_HEIGHT - (y + 1.0f) / 2.0f * WINDOW_HEIGHT - PLAYER_SPRITE_HEIGHT / 2.0f;

	SDL_Rect player_src = {0, 0, PLAYER_SPRITE_WIDTH, PLAYER_SPRITE_HEIGHT};
	SDL_FRect player_dst = {x, y, PLAYER_SPRITE_WIDTH, PLAYER_SPRITE_HEIGHT};
	SDL_RenderCopyExF(renderer, texture_atlas, &player_src, &player_dst, angle, NULL, SDL_FLIP_NONE);

	if (animate_trust) {
		thrust_x = (thrust_x + 1.0f) / 2.0f * WINDOW_WIDTH - THRUST_SPRITE_WIDTH / 2.0f;
		thrust_y = WINDOW_HEIGHT - (thrust_y + 1.0f) / 2.0f * WINDOW_HEIGHT - THRUST_SPRITE_HEIGHT / 2.0f;

		SDL_Rect animation_src = {64 + animation_frame * THRUST_SPRITE_WIDTH, 0, THRUST_SPRITE_WIDTH, THRUST_SPRITE_HEIGHT};
		SDL_FRect animation_dst = {thrust_x, thrust_y, THRUST_SPRITE_WIDTH, THRUST_SPRITE_HEIGHT};
		SDL_RenderCopyExF(renderer, texture_atlas, &animation_src, &animation_dst, angle, NULL, SDL_FLIP_NONE);
	}
}

int main() {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
		return -1;
	}

	window = SDL_CreateWindow("Asteroids", SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, 0);
	if (!window) {
		fprintf(stderr, "SDL_CreateWindow failed: %s\n", SDL_GetError());
		return -1;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (!renderer) {
		fprintf(stderr, "SDL_CreateRenderer failed: %s\n", SDL_GetError());
		return -1;
	}

	texture_atlas = IMG_LoadTexture(renderer, "assets/asteroids.png");
	if (!texture_atlas) {
		fprintf(stderr, "IMG_LoadTexture failed: %s\n", SDL_GetError());
		return -1;
	}

	for (int i = 0; i < BACKGROUND_LAYERS_COUNT; i++) {
		char buf[64] = {};

		snprintf(buf, sizeof(buf), "assets/background_%d.png", i);
		
		background_layers[i].tex = IMG_LoadTexture(renderer, buf);
		if (!background_layers[i].tex) {
			fprintf(stderr, "IMG_LoadTexture failed: %s\n", SDL_GetError());
			return -1;
		}

		int w = 0, h = 0;
		if (SDL_QueryTexture(background_layers[i].tex, NULL, NULL, &w, &h) != 0) {
			fprintf(stderr, "SDL_QueryTexture failed: %s\n", SDL_GetError());
			return -1;
		}

		background_layers[i].w = w;
		background_layers[i].h = h;
	}

	running = true;

	uint64_t ticks = SDL_GetPerformanceCounter();
	uint64_t last_ticks = 0ull;
	float dt = 0.0f;
	float time = 0;
	int frames = 0;

	bool animate_thrust = false;

	while (running) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_QUIT: {
					running = false;
				} break;

				case SDL_MOUSEMOTION: {
					mouse_x = 2.0f * event.motion.x / WINDOW_WIDTH - 1.0f;
					mouse_y = 2.0f * (WINDOW_HEIGHT - event.motion.y) / WINDOW_HEIGHT - 1.0f;
				} break;

				default:
					break;
			}
		}

		int n = 0;
		const uint8_t *k = SDL_GetKeyboardState(&n);
		assert(n >= 256);
		for (int i = 0; i < 256; i++) {
			bool was_down = keys[i].down;
			bool down = k[i];

			keys[i].down = down;
			keys[i].pressed = !was_down && down;
			keys[i].released = was_down && !down;
		}


		player_dx = mouse_x - player_x;
		player_dy = mouse_y - player_y;

		/*float dot = player_x * player_dx + player_y * player_dy;

		float mag1 = sqrtf(player_x * player_x + player_y * player_y);
		float mag2 = sqrtf(player_dx * player_dx + player_dy * player_dy);

		float angle = acosf(dot / (mag1 * mag2)) * RADIANS_TO_DEGREES;*/

		float angle = atan2f(player_dx, player_dy) * RADIAN_TO_DEGREES;

		if (keys[SDL_SCANCODE_W].down) {
			float dist = sqrtf(player_dx * player_dx + player_dy * player_dy);

			if (dist >= 0.02f) {
				float vx = player_dx / dist * PLAYER_SPEED;
				float vy = player_dy / dist * PLAYER_SPEED;

				player_x += vx * dt;
				player_y += vy * dt;

				animate_thrust = true;
			}
		} else {
			animate_thrust = false;
		}

		int thrust_animation_frame = 0;
		if (frames >= 45) {
			thrust_animation_frame = 3;
		} else if (frames >= 30) {
			thrust_animation_frame = 2;
		} else if (frames >= 15) {
			thrust_animation_frame = 1;
		} else {
			thrust_animation_frame = 0;
		}

		clear(0, 0, 0);

		for (int i = 0; i < BACKGROUND_LAYERS_COUNT; i++) {
			int layer_margin_xmax  = LAYER_MARGIN_X_INCREMENT * i;
			int layer_margin_left  = layer_margin_xmax * (mouse_x + 1.0f) / 2.0f;
			int layer_margin_right = layer_margin_xmax - layer_margin_left;

			int layer_margin_ymax   = LAYER_MARGIN_Y_INCREMENT * i;
			int layer_margin_top    = layer_margin_ymax - layer_margin_ymax * (mouse_y + 1.0f) / 2.0f;
			int layer_margin_bottom = layer_margin_ymax - layer_margin_top;

			SDL_Rect src = {layer_margin_left, layer_margin_top,
				background_layers[i].w - layer_margin_right, background_layers[i].h - layer_margin_bottom};
			SDL_Rect dst = {0, 0, WINDOW_WIDTH, WINDOW_HEIGHT};
			SDL_RenderCopy(renderer, background_layers[i].tex, &src, &dst);
		}

		line(player_x, player_y, mouse_x, mouse_y, 1.0f, 0.0f, 0.0f);

		player(player_x, player_y, angle, thrust_x, thrust_y, animate_thrust, thrust_animation_frame);

		SDL_RenderPresent(renderer);

		last_ticks = ticks;
		ticks = SDL_GetPerformanceCounter();
		dt = (ticks - last_ticks) / (float) SDL_GetPerformanceFrequency();
		
		if (dt < DESIRED_DELTA) {
			SDL_Delay((DESIRED_DELTA - dt) * 1000);
			dt = DESIRED_DELTA;

			last_ticks = ticks;
			ticks = SDL_GetPerformanceCounter();
		}

		time += dt;
		frames += 1;

		if (time >= 1.0f) {
			printf("FPS: %d\n", frames);
			time = 0.0f;
			frames = 0;
		}
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}
